<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use Throwable;

class Mobile
{

	protected $provider;

	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}

	public function makeCallByName($name = '')
	{
		if (empty($name)) return;

		$contact = $this->findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSMS($number, $body): bool
	{
		return $this->contactServiceValidateNumber($number) &&
			$this->provider->sendSMS($number, $body);
	}

	protected function findByName($name): Contact
	{
		try {
			return $this->contactServiceFindByName($name);
		} catch (Throwable $e) {
			throw new ContactNotFoundException('', 0, $e);
		}
	}

	protected function contactServiceFindByName($name): Contact
	{
		return ContactService::findByName($name);
	}

	protected function contactServiceValidateNumber($number): bool
	{
		return ContactService::validateNumber($number);
	}
}
