<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\ContactNotFoundException;
use App\Interfaces\CarrierInterface;
use App\Mobile;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{

	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = m::mock(CarrierInterface::class);

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_returns_instance_of_Call()
	{
		$provider = m::mock(CarrierInterface::class);

		$provider->shouldReceive('dialContact');

		$provider->shouldReceive('makeCall')
			->andReturn(new Call);

		$mobile = new MobileStub($provider);

		$this->assertInstanceOf(Call::class, $mobile->makeCallByName('test'));
	}

	/** @test */
	public function it_throws_ContactNotFoundException()
	{
		$this->expectException(ContactNotFoundException::class);

		$provider = m::mock(CarrierInterface::class);

		$mobile = new MobileStub($provider);

		$mobile->makeCallByName('test_null');
	}

	/** @test */
	public function it_returns_true_on_sendSMS()
	{
		$provider = m::mock(CarrierInterface::class);

		$provider->shouldReceive('sendSMS')
			->andReturn(true);

		$mobile = new MobileStub($provider);

		$this->assertTrue($mobile->sendSMS(123, 'abc'));
	}

	/** @test */
	public function it_returns_false_on_sendSMS()
	{
		$provider = m::mock(CarrierInterface::class);

		$provider->shouldReceive('sendSMS')
			->andReturn(false);

		$mobile = new MobileStub($provider);

		$this->assertFalse($mobile->sendSMS(123, 'abc'));
	}

	/** @test */
	public function it_returns_false_on_sendSMS_on_invalid_number()
	{
		$provider = m::mock(CarrierInterface::class);

		$provider->shouldReceive('sendSMS')
			->andReturn(true);

		$mobile = new MobileStub($provider);

		$this->assertFalse($mobile->sendSMS('invalid', 'abc'));
	}
}

class MobileStub extends Mobile
{
	protected function contactServicefindByName($name): Contact
	{
		return $name !== 'test_null' ? new Contact : null;
	}

	protected function contactServiceValidateNumber($number): bool
	{
		return $number !== 'invalid';
	}
}
